import "../public/build/tailwind.css";
import Vue from "vue";
import App from "./App.vue";
import VueClipboard from "vue-clipboard2";
import FlashMessage from "@smartweb/vue-flash-message";
Vue.use(FlashMessage);
Vue.use(VueClipboard);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
